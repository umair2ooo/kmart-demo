#import <UIKit/UIKit.h>
#import "AsyncImageView.h"


@protocol DeleteObject <NSObject>

-(void)method_deleteObject:(int)tag;
-(void)method_editObject:(int)tag;

@end


@interface Cell_shoppingCart : UITableViewCell
{
}

@property (weak, nonatomic) IBOutlet AsyncImageView *imageView_product;

@property (weak, nonatomic) IBOutlet UILabel *label_productName;
//@property (weak, nonatomic) IBOutlet AsyncImageView *imageView_size;
//@property (weak, nonatomic) IBOutlet UILabel *label_color;
@property (weak, nonatomic) IBOutlet UILabel *label_quantity;
@property (weak, nonatomic) IBOutlet UILabel *label_price;
@property (weak, nonatomic) IBOutlet UILabel *label_size;
@property (weak, nonatomic) IBOutlet AsyncImageView *imageView_color;
@property (weak, nonatomic) IBOutlet UIView *view_colorsAndSizes;

@property (nonatomic)id <DeleteObject> delegate;

- (IBAction)action_remove:(id)sender;
- (IBAction)action_edit:(id)sender;


@end