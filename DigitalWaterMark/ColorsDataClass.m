#import "ColorsDataClass.h"

@implementation ColorsDataClass


@synthesize color_url;
@synthesize color_uniqueID;
@synthesize color_name;
@synthesize color_array_sizes;


-(void)setColor_url:(NSString *)color_urlx
{
    if (color_urlx)
    {
        color_url = color_urlx;
    }
    else
    {
        color_url = nil;
    }
}


-(void)setColor_uniqueID:(NSString *)color_uniqueIDx
{
    if (color_uniqueIDx)
    {
        color_uniqueID = color_uniqueIDx;
    }
    else
    {
        color_uniqueID = nil;
    }
}


-(void)setColor_name:(NSString *)color_namex
{
    if (color_namex)
    {
        color_name = color_namex;
    }
    else
    {
        color_name = nil;
    }
}


-(void)setArray_sizes:(NSMutableArray *)array_sizesx
{
    if (array_sizesx)
    {
        color_array_sizes = [[NSMutableArray alloc] initWithArray:array_sizesx];
    }
}

@end