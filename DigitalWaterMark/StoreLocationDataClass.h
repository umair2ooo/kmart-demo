//
//  StoreLocationDataClass.h
//  DigitalWaterMark
//
//  Created by Fahim Bilwani on 3/5/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreLocationDataClass : NSObject

@property (nonatomic, strong)NSString *store_name;
@property (nonatomic, strong)NSString *store_address;
@property (nonatomic, strong)NSString *store_latitude;
@property (nonatomic, strong)NSString *store_longitude;
@property (nonatomic, strong)NSString *store_distance;
@property (nonatomic, strong)NSMutableArray *store_array_Details;

@end
