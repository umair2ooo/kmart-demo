//
//  SettingsViewController.h
//  Digimarc Mobile SDK: DMSDemo
//
//  Created by localTstewart on 8/15/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "scnnerViewController.h"

@interface SettingsViewController : UIViewController

@property (weak) scnnerViewController* globalViewController;

@property (retain) IBOutlet UILabel* imageProfileLabel;
@property (retain) IBOutlet UISlider* imageProfileSlider;

@property (retain) IBOutlet UILabel* audioProfileLabel;
@property (retain) IBOutlet UISlider* audioProfileSlider;

@property (retain) IBOutlet UILabel* dmsVersionLabel;

@property (retain) IBOutlet UITextField* resolverUrlString;
@property (retain) IBOutlet UIButton* btnProductionResolverURL;
@property (retain) IBOutlet UIButton* btnLabsResolverURL;

@property (retain) IBOutlet UILabel* labelPayoffsHistory;
@property (retain) IBOutlet UIButton* btnClearPayoffsHistory;

- (void) removeFromScreen;

- (IBAction)onProfileSlider:(id)sender;
- (IBAction)onButtonClick:(id)sender;

@end
