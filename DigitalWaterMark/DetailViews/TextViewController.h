//
//  TextViewController.h
//  Digimarc Mobile SDK: DMSDemo
//
//  Created by localTstewart on 9/27/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextViewController : UIViewController

@property (nonatomic, retain) NSString* contents;
@property (nonatomic, weak) IBOutlet UITextView *textView;

- (void) removeFromScreen;

@end
