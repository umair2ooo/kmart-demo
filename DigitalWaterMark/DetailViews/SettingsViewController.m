//
//  SettingsViewController.m
//  Digimarc Mobile SDK: DMSDemo
//
//  Created by localTstewart on 8/15/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import "SettingsViewController.h"
#import "DMSManager.h"

#define kProductionResolverURL  @"http://resolver.digimarc.net"
#define kLabsResolverURL        @"http://labs-resolver.digimarc.net"

#define kProfileSliderHigh  1.0
#define kProfileSliderMed   0.66
#define kProfileSliderLow   0.33
#define kProfileSliderOff   0.0

@interface SettingsViewController ()

- (void) updateUI;

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
    [self updateUI];
}

-(void)removeFromScreen {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateUI {
    DMSManager* dms = self.globalViewController.dmsManager;
    DMResolver* resolver = [DMSManager sharedDMResolver];
    
    DMSProfile imageProfile = dms.imageReadingProfile;
    DMSProfile audioProfile = dms.audioReadingProfile;
    
    self.dmsVersionLabel.text = [NSString stringWithFormat:@"DMSDK %@", [dms version]];

    self.imageProfileLabel.text = [DMSManager stringFromDMSProfile:imageProfile];
    self.audioProfileLabel.text = [DMSManager stringFromDMSProfile:audioProfile];
    
    float sliderValue;
    switch(imageProfile) {
        case DMSProfileHigh:     sliderValue = kProfileSliderHigh;   break;
        case DMSProfileMedium:   sliderValue = kProfileSliderMed;    break;
        case DMSProfileLow:      sliderValue = kProfileSliderLow;    break;
        case DMSProfileIdle:
        default:                    sliderValue = kProfileSliderOff;    break;
    }
    [self.imageProfileSlider setValue:sliderValue animated:YES];
    
    switch(audioProfile) {
        case DMSProfileHigh:     sliderValue = kProfileSliderHigh;   break;
        case DMSProfileMedium:   sliderValue = kProfileSliderMed;    break;
        case DMSProfileLow:      sliderValue = kProfileSliderLow;    break;
        case DMSProfileIdle:
        default:                    sliderValue = kProfileSliderOff;    break;
    }
    [self.audioProfileSlider setValue:sliderValue animated:YES];
    
    self.resolverUrlString.text = resolver.serviceURL.absoluteString;
    
    self.labelPayoffsHistory.text = [NSString stringWithFormat:@"Payoffs History: %lu items",
                                     (unsigned long)[self.globalViewController payoffsHistoryCount] ];
}

- (IBAction)onProfileSlider:(id)sender
{
    DMSManager* dms = self.globalViewController.dmsManager;
    if(sender == self.imageProfileSlider) {
        float  newValue = self.imageProfileSlider.value;
        if(newValue > 0.75) {
            dms.imageReadingProfile = DMSProfileHigh;
        } else if(newValue > 0.50) {
            dms.imageReadingProfile = DMSProfileMedium;
        } else if(newValue > 0.25) {
            dms.imageReadingProfile = DMSProfileLow;
        } else {
            dms.imageReadingProfile = DMSProfileIdle;
        }
    } else if(sender == self.audioProfileSlider) {
        float  newValue = self.audioProfileSlider.value;
        if(newValue > 0.75) {
            dms.audioReadingProfile = DMSProfileHigh;
        } else if(newValue > 0.50) {
            dms.audioReadingProfile = DMSProfileMedium;
        } else if(newValue > 0.25) {
            dms.audioReadingProfile = DMSProfileLow;
        } else {
            dms.audioReadingProfile = DMSProfileIdle;
        }
    }
    
    [self updateUI];
}

- (IBAction)onButtonClick:(id)sender {
    DMResolver* resolver = [DMSManager sharedDMResolver];
    if(sender == self.btnProductionResolverURL) {
        resolver.serviceURL = [NSURL URLWithString:kProductionResolverURL];
    } else if(sender == self.btnLabsResolverURL) {
        resolver.serviceURL = [NSURL URLWithString:kLabsResolverURL];
    } else if(sender == self.btnClearPayoffsHistory) {
        [self.globalViewController clearPayoffsHistory];
    }
    [self updateUI];
}

@end
