#import "TutorialViewController.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:HELP_BACKGROUND_COLOUR];
    
    CGFloat labelSize = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ? 80.0 : 160.0;
    CGFloat fontSize = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ? 18 : 28;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [backButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [backButton setTitle:@"Done" forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(removeFromScreen) forControlEvents:UIControlEventTouchUpInside];
    backButton.titleLabel.font = [UIFont systemFontOfSize:[[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ? 20 : 28];
    [self.view addSubview:backButton];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:backButton
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1.0
                                                              constant:0.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:backButton
                                                             attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1.0
                                                              constant:0.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:backButton
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0
                                                              constant:labelSize
                                 ],
                                [NSLayoutConstraint constraintWithItem:backButton
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0
                                                              constant:labelSize
                                 ]
                                ]
     ];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.text = TUTORIAL_TITLE;
    titleLabel.font = [UIFont boldSystemFontOfSize:fontSize*1.5];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.numberOfLines = 2;
    [self.view addSubview:titleLabel];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:titleLabel
                                                             attribute:NSLayoutAttributeCenterY
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:backButton
                                                             attribute:NSLayoutAttributeCenterY
                                                            multiplier:1.0
                                                              constant:0.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:titleLabel
                                                             attribute:NSLayoutAttributeLeft
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeLeft
                                                            multiplier:1.0
                                                              constant:10.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:titleLabel
                                                             attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1.0
                                                              constant:-10.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:titleLabel
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0
                                                              constant:labelSize*0.6
                                 ]
                                ]
     ];
    
    UILabel *tutorialLabel = [[UILabel alloc] init];
    tutorialLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSMutableParagraphStyle *centerParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    centerParagraphStyle.alignment = NSTextAlignmentCenter;
    NSMutableParagraphStyle *leftParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    leftParagraphStyle.alignment = NSTextAlignmentLeft;
    NSMutableAttributedString *mutableAttrString = [[NSMutableAttributedString alloc] init];
    NSArray *stringArray = [TUTORIAL_TEXT componentsSeparatedByString:@"|"];
    [mutableAttrString appendAttributedString:[[NSAttributedString alloc] initWithString:stringArray[0]
                                                                              attributes:@{
                                                                                           NSParagraphStyleAttributeName:   centerParagraphStyle,
                                                                                           NSFontAttributeName:             [UIFont boldSystemFontOfSize:fontSize],
                                                                                           }
                                               ]
     ];
    [mutableAttrString appendAttributedString:[[NSAttributedString alloc] initWithString:stringArray[1]
                                                                              attributes:@{
                                                                                           NSParagraphStyleAttributeName:   leftParagraphStyle,
                                                                                           NSFontAttributeName:             [UIFont systemFontOfSize:fontSize]
                                                                                           }
                                               ]
     ];
    [mutableAttrString appendAttributedString:[[NSAttributedString alloc] initWithString:stringArray[2]
                                                                              attributes:@{
                                                                                           NSParagraphStyleAttributeName:   centerParagraphStyle,
                                                                                           NSFontAttributeName:             [UIFont boldSystemFontOfSize:fontSize],
                                                                                           }
                                               ]
     ];
    [mutableAttrString appendAttributedString:[[NSAttributedString alloc] initWithString:stringArray[3]
                                                                              attributes:@{
                                                                                           NSParagraphStyleAttributeName:   leftParagraphStyle,
                                                                                           NSFontAttributeName:             [UIFont systemFontOfSize:fontSize]
                                                                                           }
                                               ]
     ];
    [tutorialLabel setAttributedText:mutableAttrString];
    tutorialLabel.numberOfLines = CGFLOAT_MAX;
    tutorialLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:tutorialLabel];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:tutorialLabel
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:titleLabel
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1.0
                                                              constant:0.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:tutorialLabel
                                                             attribute:NSLayoutAttributeLeft
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeLeft
                                                            multiplier:1.0
                                                              constant:10.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:tutorialLabel
                                                             attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1.0
                                                              constant:-10.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:tutorialLabel
                                                             attribute:NSLayoutAttributeCenterY
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterY
                                                            multiplier:1.0
                                                              constant:0.0
                                 ],
                                ]
     ];
}

-(void)removeFromScreen {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
