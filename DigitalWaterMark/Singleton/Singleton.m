#import "Singleton.h"
//#import "Reachability.h"

@implementation Singleton

@synthesize bool_isFollowModule = _bool_isFollowModule;
@synthesize string_imageURL = _string_imageURL;
@synthesize image_ARImage = _image_ARImage;
@synthesize array_colorsImages = _array_colorsImages;
@synthesize array_sizesImages = _array_sizesImages;
@synthesize productDetail = _productDetail;
@synthesize string_exitSegueIdentifier = _string_exitSegueIdentifier;
@synthesize string_WCToken = _string_WCToken;
@synthesize string_WCTrustedToken = _string_WCTrustedToken;
@synthesize string_orderId = _string_orderId;
@synthesize array_cartObjects  = _array_cartObjects;
@synthesize string_singleSKUUniqueID = _string_singleSKUUniqueID;


static Singleton *sharedSingleton = nil;

+(Singleton *)retriveSingleton
{
	@synchronized(self)
	{
		if(sharedSingleton ==nil)
		{
			sharedSingleton =[[Singleton alloc]init];
		}
	}
	return sharedSingleton;
}

+(id)allocWithZone:(NSZone *)zone
{
	@synchronized(self)
	{
		if(sharedSingleton ==nil)
		{
			sharedSingleton = [super allocWithZone:zone];
			return sharedSingleton;
		}
	}
	return nil;
}

//-(BOOL)method_connectToInternet
//{
//    Reachability *r = [Reachability reachabilityWithHostName:@"www.google.com"];
//    NetworkStatus internetStatus = [r currentReachabilityStatus];    
//    
//    if (internetStatus == ReachableViaWiFi)
//    {
//        return YES;
//    }
//    else if(internetStatus == ReachableViaWWAN)
//    {
//        return YES;
//    }
//    return  NO;
//}

#pragma mark - is email address validity?
-(BOOL)method_NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - are all spaces?
-(BOOL)method_checkOnlySpaces:(NSString *)str
{
    BOOL isValid = false;
    
    NSString *rawString = str;
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    
    if ([trimmed length] > 0)
    {
        isValid = true;
    }
    
    return isValid;
}
#pragma mark - phone number validation
-(BOOL)method_phoneNumberValidation:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}
@end