#import "GetProductDataFromServer.h"

#import "AFNetworking.h"
#import "Singleton.h"
#import "ProductsDataClass.h"
#import "ColorsDataClass.h"
#import "LocationDataClass.h"
#import "StoreLocationDataClass.h"
#import "ActivityIndicator.h"
#import "scnnerViewController.h"

//#define k_localAddress @"http://192.168.16.38/"
//#define k_publicAddress @"http://119.63.131.227/"
//https://arandell.royalcyber.com/webapp/wcs/stores/servlet/en/aurora
//    http://119.63.131.227/webapp/wcs/stores/servlet/en/aurora
//    http://192.168.16.38/webapp/wcs/stores/servlet/en/aurora



@implementation GetProductDataFromServer

@synthesize delegate = _delegate;

#pragma mark - method_makeIDFromPayLoad
-(void)method_makeIDFromPayLoad:(id)id_
{
    DLog(@"id: %@", id_);


//    [self method_fetchData:@"16455"];
    
    
//    16451             Jackeroo Basic Camp Table
//    16453             Audiosonic 10.1' Portable DVD Player
//    16455             Grey Bunny Folding Plush
    
    
    
    

    /////////                                                           Arandell
    
    
    if ([id_ isEqualToString:@"7794"])//shoe
    {
        [self method_fetchData:@"10003"];
    }
    else if ([id_ isEqualToString:@"6565"])//orange jacket
    {
        [self method_fetchData:@"10002"];
    }
    else if ([id_ isEqualToString:@"6986"])//wrist watch
    {
        [self method_fetchData:@"10004"];
    }
    else if ([id_ isEqualToString:@"78954"])//black jacket
    {
        [self method_fetchData:@"10005"];
    }
    else if ([id_ isEqualToString:@"4325"])//Bed
    {
        [self method_fetchData:@"10006"];
    }
    
    
    
    //////////////////////////              for bed room
    
    //    Beautiful Double Bed
    //    15951
    //
    //    Light Ball
    //    15952
    //
    //    Artframe
    //    15957
    //
    //    Wall Tree
    //    15959
    //
    //    White Flower Vase
    //    15955
    
    
    
    
    
    /////////                                                           CaptureLife
    
    
    
    else if ([id_ isEqualToString:@"15951"] || [id_ isEqualToString:@"15952"] || [id_ isEqualToString:@"15955"] || [id_ isEqualToString:@"15957"] || [id_ isEqualToString:@"15959"])
    {
        [self method_fetchData:id_];
    }
    
    
    
    
    /////////                                                               kMart
    
    
    
    
    else if ([id_ isEqualToString:@"16451"] || [id_ isEqualToString:@"16453"] || [id_ isEqualToString:@"16455"])
    {
        [self method_fetchData:id_];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Product doesn't exists in store"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
}


#pragma mark - method_fetchData
- (void)method_fetchData:(NSString *)id_                // it gets the complete product  detail from server
{
    [[ActivityIndicator currentIndicator] showAfterDelay];
    Singleton *single = [Singleton retriveSingleton];
    //New method
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", nil];
    requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/productview/byId/%@", k_url, id_];//14451//13914

    DLog(@"%@", strUrl);


    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [requestManager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

         
         if (error != nil)
         {
             [[ActivityIndicator currentIndicator] hideAfterDelay];
             NSLog(@"error: %@", error);
         }
         else
         {
             NSLog(@"json: %@", json);
             if (single.productDetail)
             {
                 single.productDetail = nil;
                 single.string_singleSKUUniqueID = nil;
             }


             
             
             
             /////////////////////   colors and sizes

             NSMutableArray *array_colors = [[NSMutableArray alloc] init];

             if ([[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_Attributes])
             {
                 if ([[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_Attributes] count])
                 {
                     
                     [(NSArray *)[[[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_Attributes] objectAtIndex:0] valueForKey:@k_Values] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                         
                         [array_colors addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                  [obj valueForKey:@k_uniqueID], @"uniqueID",
                                                  [obj valueForKey:@k_values], @"values", nil]];
                     }];
                 }
             }
             
             
             //                 DLog(@"%@", array_colors);
             
             
             
             NSMutableArray *array_tempColor = [[NSMutableArray alloc] init];
             
             if ([[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_SKUs])
             {
                 if ([[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_SKUs] count])
                 {
                     
// total number of colors loop
                     
                     [array_colors enumerateObjectsUsingBlock:^(id obj_, NSUInteger idx, BOOL *stop) {
                         
                         __block int indexOfColor = 0;
                         
                         
// different sizes for each color loop
                         
                         [(NSArray *)[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_SKUs] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                             
                             
                             
                             indexOfColor = idx;
                             
                             
                             if ([[[[[[obj valueForKey:@k_Attributes] objectAtIndex:1] valueForKey:@k_Values] objectAtIndex:0] valueForKey:@k_uniqueID] isEqualToString:[obj_ valueForKey:@k_uniqueID]])
                             {
                                 

                                 // it means colors and sizes exists, and here we picked the size, price and sku unique id of each color
                                 
                                 
                                 [array_tempColor addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                             
                                                             [obj valueForKey:@K_SKUUniqueID], @K_SKUUniqueID,
                                                             [[[obj valueForKey:@"Price"] objectAtIndex:0] valueForKey:K_SKUPriceValue], K_SKUPriceValue,
                                                             [[[[[obj valueForKey:@k_Attributes] objectAtIndex:0] valueForKey:@k_Values] objectAtIndex:0] valueForKey:@k_values], @k_size, nil]];
                                 
                                 
                                 DLog(@"%@", array_tempColor);
                             }
                         }];



                         if (indexOfColor == [[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_SKUs] count]-1)
                         {

                             DLog(@"%@", obj_);
                             
                             NSMutableDictionary *dic_temp = [NSMutableDictionary dictionaryWithDictionary:obj_];
                             
                             
                             [dic_temp setObject:[NSArray arrayWithArray:array_tempColor] forKey:@"sizes"];
                             
                             
                             [array_colors replaceObjectAtIndex:[array_colors indexOfObject:obj_] withObject:[NSDictionary dictionaryWithDictionary:dic_temp]];
                             
                             
                             dic_temp = nil;
                             
                             [array_tempColor removeAllObjects];
                             indexOfColor = 0;
                         }
                     }];
                 }
             }
             
             
             
             [array_colors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                 
                 NSPredicate *predicate = [NSPredicate predicateWithFormat:@"path CONTAINS[cd] %@", [obj valueForKey:@k_values]];
                 
                 if ([[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@"Attachments"])
                 {
                     NSArray *aNames = [[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@"Attachments"] filteredArrayUsingPredicate:predicate];
                     
                     
                     NSMutableDictionary *dic_temp = [NSMutableDictionary dictionaryWithDictionary:[array_colors objectAtIndex:[array_colors indexOfObject:obj]]];
                     [dic_temp setObject:[[aNames objectAtIndex:0] valueForKey:@"path"] forKey:@"colorURL"];

                     [array_colors replaceObjectAtIndex:[array_colors indexOfObject:obj] withObject:dic_temp];

                     dic_temp = nil;
                 }
             }];
             
             
             //                 DLog(@"%@", array_colors);
             
             
             /////////////////////  colors and sizes
             
             
             
             
             
             
             
             
             
             
             ProductsDataClass *obj_ = [[ProductsDataClass alloc] init];
             
             
             [obj_ setP_array_colors:array_colors];
             
             
             [obj_ setP_id:[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:k_id]];
             [obj_ setP_name:[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@k_productName]];
             [obj_ setP_price:[[[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@"Price"] objectAtIndex:0] valueForKey:@k_price]];
             [obj_ setP_description:[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@k_productDesc]];
             
             
             
             
             NSString *imageUrl = [NSString stringWithFormat:@"%@%@",k_url,[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@"thumbnail"]];
             [obj_ setP_imageURL:[NSURL URLWithString:imageUrl]];
             imageUrl = nil;
             
             
             imageUrl = [NSString stringWithFormat:@"%@%@",k_url,[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@k_detailImage]];
             [obj_ setP_imageURL_AR:[NSURL URLWithString:imageUrl]];
             imageUrl = nil;
             
             single.productDetail = obj_;
             
             if ([[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:k_singleSKUUniqueID])
             {
                 DLog(@"It means there are no multiple choices for this product");
                 
                 single.string_singleSKUUniqueID = [[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:k_singleSKUUniqueID];
             }
             
             obj_ = nil;
             
             
             if (single.productDetail)
             {
                 single.string_imageURL = nil;
                 single.image_ARImage = nil;
  
                 [self.delegate method_dataWithParams:nil
                                                  dic:nil
                                            isSuccess:YES 
                                          serviceName:k_webServiceGetMainProduct];
             }
             else
             {
                 [[[UIAlertView alloc] initWithTitle:nil
                                             message:@"Product not matched"
                                            delegate:nil
                                   cancelButtonTitle:@"Ok"
                                   otherButtonTitles:nil, nil]
                  show];
             }
             [[ActivityIndicator currentIndicator] hideAfterDelay];
         }
     }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [[ActivityIndicator currentIndicator] hideAfterDelay];
                }];
}





#pragma mark - method_fetchLocation
- (void)method_fetchLocation:(NSString *)id_ City:(NSString *)city State:(NSString *)state Miles:(NSString *)miles  // it gets the stores locations from server
{
    __block NSMutableArray *array_stores = [[NSMutableArray alloc] init];
    
    //http://maps.googleapis.com/maps/api/geocode/json?address=T2G%205R1
    //http://192.168.16.38/wcs/resources/store/10001/storelocator/latitude/51.0499705/longitude/-114.0565018?maxItems=&siteLevelStoreSearch=false&responseFormat=json
    
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", nil];
    requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString *strUrl;
    if ([state length])
    {
        strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/storelocator/byLocation?city=%@&state=%@&siteLevelStoreSearch=false&radius=%@",k_url,city,state,miles];
    }
    else
    {
        strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/storelocator/byLocation?city=%@&siteLevelStoreSearch=false&radius=%@",k_url,city,miles];
    }

    DLog(@"strUrl:%@",strUrl);
    
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [requestManager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
         
         if (error != nil)
         {
             [[ActivityIndicator currentIndicator] show];
             NSLog(@"json: %@", json);
         }
         else
         {
             NSLog(@"Array: %@", json);
             if ([json valueForKey:@k_LocatioData])
             {
                 [(NSArray *)[json valueForKey:@k_LocatioData] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
                 {
                     
                     DLog(@"%@", obj);
                     DLog(@"%@", [obj valueForKey:k_storeLocName]);
                     
                     
                     StoreLocationDataClass *obj_ = [[StoreLocationDataClass alloc] init];
                     [obj_ setStore_name:[obj valueForKey:k_storeLocName]];
                     [obj_ setStore_latitude:[obj valueForKey:k_latitude]];
                     [obj_ setStore_longitude:[obj valueForKey:k_longitude]];
                     [obj_ setStore_address:[[obj valueForKey:k_address] objectAtIndex:0]];
                     
                     DLog(@"%@", obj_.store_name);
                     
                     [array_stores addObject:obj_];
                     
                     obj_ = nil;
                 }];
                 
                 

                 [self.delegate method_dataWithParams:array_stores dic:nil isSuccess:YES serviceName:nil];
//                 [self.delegate method_dataWithParams:array_stores dic:nil serviceName:nil];
                 
                 array_stores = nil;
                 
//                 [obj_ setStore_array_Details:array_stores];
             }
             else
             {
                 [self.delegate method_failureResponse];
             }
         }
     }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    DLog(@"No reponse");
                    [self.delegate method_failureResponse];
                }];
}



-(void)method_fetchLocationByZipcode_Latitude:(NSString *)lat Longitude:(NSString *)lon     // it gets the location by zip code from server
{
    __block NSMutableArray *array_stores = [[NSMutableArray alloc] init];
    
    //http://maps.googleapis.com/maps/api/geocode/json?address=T2G%205R1
    //http://192.168.16.38/wcs/resources/store/10001/storelocator/latitude/51.0499705/longitude/-114.0565018?maxItems=&siteLevelStoreSearch=false&responseFormat=json
    
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", nil];
    requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/storelocator/latitude/%@/longitude/%@?maxItems=&siteLevelStoreSearch=false&responseFormat=json",k_url,lat,lon];
    DLog(@"strUrl:%@",strUrl);
    
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [requestManager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
         
         if (error != nil)
         {
             NSLog(@"json: %@", json);
         }
         else
         {
             NSLog(@"Array: %@", json);
             if ([json valueForKey:@k_LocatioData])
             {
                 [(NSArray *)[json valueForKey:@k_LocatioData] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                     StoreLocationDataClass *obj_ = [[StoreLocationDataClass alloc] init];
                     [obj_ setStore_name:[obj valueForKey:k_storeLocName]];
                     [obj_ setStore_latitude:[obj valueForKey:k_latitude]];
                     [obj_ setStore_longitude:[obj valueForKey:k_longitude]];
                     [obj_ setStore_address:[[obj valueForKey:k_address] objectAtIndex:0]];
                     
                     [array_stores addObject:obj_];
                     
                     obj_ = nil;
                 }];
  
                 [self.delegate method_dataWithParams:array_stores dic:nil isSuccess:YES serviceName:nil];
//                 [self.delegate method_dataWithParams:array_stores dic:nil serviceName:nil];
                 
                 array_stores = nil;
             }
             else
             {
                 [self.delegate method_failureResponse];
                 
                 
//                 [[[UIAlertView alloc] initWithTitle:@"Failure"
//                                             message:@"Store(s) not found."
//                                            delegate:nil
//                                   cancelButtonTitle:@"Ok"
//                                   otherButtonTitles:nil, nil]
//                  show];
//
             }
         }
     }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    DLog(@"No reponse");
                    [self.delegate method_failureResponse];
                }];

}

-(void)method_autocompleteAddress_input:(NSString *)input
{
    
    __block NSMutableArray *array_predictions = [[NSMutableArray alloc] init];
    
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    
    [[requestManager operationQueue] cancelAllOperations];
    
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", nil];
    requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&key=AIzaSyBvvEaJQjZB83UqmTduXI-F6jMk75QL9OE",input];
    DLog(@"strUrl:%@",strUrl);
    
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [requestManager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
         
         if (error != nil)
         {
             NSLog(@"json: %@", json);
         }
         else
         {
             NSLog(@"Array: %@", json);
             if ([json valueForKey:@"predictions"])
             {
                 [(NSArray *)[json valueForKey:@"predictions"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
                 {
                     [array_predictions addObject:[obj valueForKey:@"description"]];
                 }];
                 
                 [self.delegate method_dataWithParams:array_predictions dic:nil isSuccess:YES serviceName:nil];
                 array_predictions = nil;
             }
             else
             {
                 [self.delegate method_failureResponse];
             }
         }
     }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [self.delegate method_failureResponse];
                }];
    
}



#pragma mark - method_addToCart
-(void)method_addToCart:(NSDictionary *)dic serviceName:(NSString*)serviceName         // adding the Cart to the shopping cart list to the server
{
    Singleton *single= [Singleton retriveSingleton];
    [[ActivityIndicator currentIndicator] show];
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    
    requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    requestManager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [requestManager.requestSerializer setValue:single.string_WCToken forHTTPHeaderField:@"WCToken"];
    [requestManager.requestSerializer setValue:single.string_WCTrustedToken forHTTPHeaderField:@"WCTrustedToken"];
    [requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    
//    DLog(@"%@", single.string_WCToken);
//    DLog(@"%@", single.string_WCTrustedToken);
    DLog(@"%@", dic);


    [requestManager POST:[NSString stringWithFormat:@"%@wcs/resources/store/10001/cart?responseFormat=json", k_url]
              parameters:dic
                 success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

         if (error != nil)
         {
             NSLog(@"json: %@", json);
             [[ActivityIndicator currentIndicator] hide];
             [[[UIAlertView alloc] initWithTitle:nil
                                         message:@"Item does not added, please try later"
                                        delegate:nil
                               cancelButtonTitle:@"Ok"
                               otherButtonTitles:nil, nil]
              show];
         }
         else
         {
             NSLog(@"Array: %@", json);
             [[ActivityIndicator currentIndicator] hide];
             if ([json valueForKey:@"orderId"])
             {
                 if (!single.string_orderId)
                 {
                     single.string_orderId = [json valueForKey:@"orderId"];
                 }

                 
                 [self.delegate method_dataWithParams:nil dic:json isSuccess:YES serviceName:serviceName];
                 
//                 [self.delegate method_isDataSucess:YES];
//                 
//                 [self.delegate method_dataWithParams:nil dic:json serviceName:nil];
             }
             else
             {
                 [[[UIAlertView alloc] initWithTitle:nil
                                             message:@"Item does not added, please try later"
                                            delegate:nil
                                   cancelButtonTitle:@"Ok"
                                   otherButtonTitles:nil, nil]
                  show];
             }
         }
     }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {

                     DLog(@"%@", error);
                     [[ActivityIndicator currentIndicator] hide];
                     [[[UIAlertView alloc] initWithTitle:nil
                                                 message:@"Item does not added, please try later"
                                                delegate:nil
                                       cancelButtonTitle:@"Ok"
                                       otherButtonTitles:nil, nil]
                      show];
                 }];
}



#pragma mark - shopping cart list
-(void)method_shoppingCartList_serviceName:(NSString*)serviceName              // it gets the complete shopping cart list from server
{
    Singleton *single = [Singleton retriveSingleton];
    
    __block NSMutableArray *array_cartList = [[NSMutableArray alloc] init];
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    [requestManager.requestSerializer setValue:single.string_WCToken forHTTPHeaderField:k_WCToken];
    [requestManager.requestSerializer setValue:single.string_WCTrustedToken forHTTPHeaderField:k_WCTrustedToken];
    [requestManager.requestSerializer setValue:single.string_orderId forHTTPHeaderField:@"sortOrderItemBy"];
    
    [requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/cart/@self", k_url];
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    
    [requestManager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         DLog(@"%@", responseObject);

             if ([responseObject isKindOfClass:[NSDictionary class]])
             {
                 [(NSArray *)[responseObject valueForKey:@k_orderItem] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                     ProductsDataClass *obj_ = [[ProductsDataClass alloc] init];
                     [obj_ setP_price:[obj valueForKey:k_orderItemPrice]];
                     [obj_ setP_quantity:[obj valueForKey:k_quantity]];
                     [obj_ setP_orderItemId:[obj valueForKey:k_orderItemId]];
                     [obj_ setP_id:[obj valueForKey:k_productId]];
                     [array_cartList addObject:obj_];
                     
                     obj_ = nil;
                 }];
                 ProductsDataClass *obj_ = [[ProductsDataClass alloc] init];
                 [obj_ setP_subTotal:[responseObject valueForKey:k_grandTotal]];
                 [obj_ setP_subTotalCurrency:[responseObject valueForKey:k_grandTotalCurrency]];
                 
                
//                 [self.delegate method_dataWithParams:array_cartList dic:[NSDictionary dictionaryWithObjectsAndKeys:obj_.p_subTotal,@"grandTotal",obj_.p_subTotalCurrency,@"grandTotalCurrency",nil]];
                 
                 [self.delegate method_dataWithParams:array_cartList dic:[NSDictionary dictionaryWithObjectsAndKeys:obj_.p_subTotal,@"grandTotal",obj_.p_subTotalCurrency,@"grandTotalCurrency",nil] isSuccess:YES serviceName:serviceName];
                 
//                 [self.delegate method_dataWithParams:array_cartList
//                                                  dic:[NSDictionary dictionaryWithObjectsAndKeys:obj_.p_subTotal,@"grandTotal",obj_.p_subTotalCurrency,@"grandTotalCurrency",nil] serviceName:nil];
                 
                 
                 array_cartList = nil;
             }
             else
             {
                 [[[UIAlertView alloc] initWithTitle:nil
                                             message:@"No item available"
                                            delegate:nil
                                   cancelButtonTitle:@"Ok"
                                   otherButtonTitles:nil, nil]
                  show];
             }
    }
                failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
                    DLog(@"%@", error);
                    [self.delegate method_failureResponse];
                    [[[UIAlertView alloc] initWithTitle:nil
                                                message:@"No item available"
                                               delegate:nil
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil, nil]
                     show];
                }];
}



#pragma mark - method_fetchDataToEditProduct
-(void)method_fetchDataToEditProduct:(id)id_            // it gets the complete product  detail from server
{
    //New method
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", nil];
    requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/productview/byId/10002", k_url];//14451


    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [requestManager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

         if (error != nil)
         {
             NSLog(@"json: %@", json);
         }
         else
         {
             NSLog(@"Array: %@", json);


             /////////////////////   colors and sizes

             NSMutableArray *array_colors = [[NSMutableArray alloc] init];

             if ([[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_Attributes])
             {
                 if ([[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_Attributes] count])
                 {
                     
                     [(NSArray *)[[[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_Attributes] objectAtIndex:0] valueForKey:@k_Values] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                         
                         [array_colors addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                  [obj valueForKey:@k_uniqueID], @"uniqueID",
                                                  [obj valueForKey:@k_values], @"values", nil]];
                     }];
                 }
             }
             
             
             //                 DLog(@"%@", array_colors);
             
             
             
             NSMutableArray *array_tempColor = [[NSMutableArray alloc] init];
             
             if ([[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_SKUs])
             {
                 if ([[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_SKUs] count])
                 {
                     
                     // total number of colors loop
                     
                     [array_colors enumerateObjectsUsingBlock:^(id obj_, NSUInteger idx, BOOL *stop) {
                         
                         __block int indexOfColor = 0;
                         
                         
                         // different sizes for each color loop
                         
                         [(NSArray *)[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_SKUs] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                             
                             
                             
                             indexOfColor = idx;
                             
                             
                             if ([[[[[[obj valueForKey:@k_Attributes] objectAtIndex:1] valueForKey:@k_Values] objectAtIndex:0] valueForKey:@k_uniqueID] isEqualToString:[obj_ valueForKey:@k_uniqueID]])
                             {
                                 
                                 // here we picked the size of each color
                                 
                                 [array_tempColor addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                             
                                                             [obj valueForKey:@K_SKUUniqueID], @K_SKUUniqueID,
                                                             [[[[[obj valueForKey:@k_Attributes] objectAtIndex:0] valueForKey:@k_Values] objectAtIndex:0] valueForKey:@k_values], @k_size, nil]];
                                 
                                 
                                 DLog(@"%@", array_tempColor);
                             }
                         }];
                         
                         
                         
                         if (indexOfColor == [[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@k_SKUs] count]-1)
                         {
                             
                             DLog(@"%@", obj_);

                             
                             NSMutableDictionary *dic_temp = [NSMutableDictionary dictionaryWithDictionary:obj_];
                             
                             
                             [dic_temp setObject:[NSArray arrayWithArray:array_tempColor] forKey:@"sizes"];
                             
                             
                             [array_colors replaceObjectAtIndex:[array_colors indexOfObject:obj_] withObject:[NSDictionary dictionaryWithDictionary:dic_temp]];
                             
                             //                                 DLog(@"%@", array_colors);
                             
                             dic_temp = nil;
                             
                             [array_tempColor removeAllObjects];
                             indexOfColor = 0;
                             
                             //                                 DLog(@"%@", array_colors);
                         }
                     }];
                 }
             }
             
             
             
             [array_colors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                 
                 NSPredicate *predicate = [NSPredicate predicateWithFormat:@"path CONTAINS[cd] %@", [obj valueForKey:@k_values]];
                 
                 NSArray *aNames = [[[[json valueForKey:@k_CatalogEntryView] objectAtIndex:0] valueForKey:@"Attachments"] filteredArrayUsingPredicate:predicate];
                 
                 
                 NSMutableDictionary *dic_temp = [NSMutableDictionary dictionaryWithDictionary:[array_colors objectAtIndex:[array_colors indexOfObject:obj]]];
                 [dic_temp setObject:[[aNames objectAtIndex:0] valueForKey:@"path"] forKey:@"colorURL"];
                 
                 [array_colors replaceObjectAtIndex:[array_colors indexOfObject:obj] withObject:dic_temp];
                 
                 dic_temp = nil;
             }];
             
             
             //                 DLog(@"%@", array_colors);
             
             
             /////////////////////  colors and sizes
             
             ProductsDataClass *obj_ = [[ProductsDataClass alloc] init];
             
             [obj_ setP_array_colors:array_colors];
             [obj_ setP_id:[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:k_id]];
             [obj_ setP_name:[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@k_productName]];
             [obj_ setP_price:[[[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@"Price"] objectAtIndex:0] valueForKey:@k_price]];
             [obj_ setP_description:[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@k_productDesc]];
             NSString *imageUrl = [NSString stringWithFormat:@"%@%@",k_url,[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@k_detailImage]];
             [obj_ setP_imageURL:[NSURL URLWithString:imageUrl]];
             [obj_ setP_imageURL_AR:[NSURL URLWithString:imageUrl]];
             
             if (obj_)
             {
                 [self.delegate method_dataFromServer:obj_];
             }
             else
             {
                 [[[UIAlertView alloc] initWithTitle:nil
                                             message:@"Product not matched"
                                            delegate:nil
                                   cancelButtonTitle:@"Ok"
                                   otherButtonTitles:nil, nil]
                  show];
             }
         }
     }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                }];

}


#pragma mark - method_guestLogin
-(void)method_guestLogin                            // making the session with the server or we can say the guest user
{
    Singleton *single = [Singleton retriveSingleton];
    
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    securityPolicy.validatesCertificateChain = NO;


    // This usually would be a subclass of AFHTTPSessionManager
    AFHTTPSessionManager *client = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:k_url] sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    client.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    client.securityPolicy.allowInvalidCertificates = YES;


    requestManager.responseSerializer = [AFJSONResponseSerializer serializer];
    requestManager.requestSerializer = [AFJSONRequestSerializer serializer];
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];


    [requestManager POST:[NSString stringWithFormat:@"%@wcs/resources/store/10001/guestidentity", k_url]
              parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         DLog(@"%@", responseObject);
         
         if ([responseObject valueForKey:k_WCToken])
         {
             single.string_WCToken = [responseObject valueForKey:k_WCToken];
             single.string_WCTrustedToken = [responseObject valueForKey:k_WCTrustedToken];
             
//             scnnerViewController *obj_scnnerViewController = (scnnerViewController*)[[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier: @"scnnerViewController"];
//             [obj_scnnerViewController method_getPromotionCode];
             
             DLog(@"WCToken = %@", single.string_WCToken);
             DLog(@"WCTrustedToken = %@", single.string_WCTrustedToken);
         }
         else
         {
             [[[UIAlertView alloc] initWithTitle:nil
                                         message:@"Authentication failed"
                                        delegate:nil
                               cancelButtonTitle:@"Ok"
                               otherButtonTitles:nil, nil]
              show];
         }
     }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     
                     DLog(@"%@", error);
                     
                     [[[UIAlertView alloc] initWithTitle:nil
                                                 message:@"Authentication failed"
                                                delegate:nil
                                       cancelButtonTitle:@"Ok"
                                       otherButtonTitles:nil, nil]
                      show];
                 }];
}



#pragma mark - method_editShoppingCart
-(void)method_editShoppingCart:(NSDictionary *)dic serviceName:(NSString*)serviceName          // it gets the complete product  detail from server
{
    DLog(@"%@", dic);
    
    [[ActivityIndicator currentIndicator] show];
    
    
    Singleton *single = [Singleton retriveSingleton];
    
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    
    requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    requestManager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [requestManager.requestSerializer setValue:single.string_WCToken forHTTPHeaderField:@"WCToken"];
    [requestManager.requestSerializer setValue:single.string_WCTrustedToken forHTTPHeaderField:@"WCTrustedToken"];
    [requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    

    [requestManager PUT:[NSString stringWithFormat:@"%@wcs/resources/store/10001/cart/@self", k_url]
              parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
         
         [[ActivityIndicator currentIndicator] hide];
         
         NSLog(@"json: %@", json);
         
         if (error != nil)
         {
             DLog(@"%@", error);
             
             [self.delegate method_dataWithParams:nil dic:nil isSuccess:NO serviceName:k_webServiceEditCart];
         }
         else
         {
             [self.delegate method_dataWithParams:nil dic:nil isSuccess:YES serviceName:k_webServiceEditCart];
             
//             [self.delegate method_isDataSucess:YES];
         }
     }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     
                     DLog(@"%@", error);
                     
                     [[ActivityIndicator currentIndicator] hide];

                     [self.delegate method_failureResponse];
                 }];
}




#pragma mark - method_removeShoppingCart
-(void)method_removeShoppingCart:(NSDictionary *)dic serviceName:(NSString*)serviceName            // removing the shopping cart from server
{
//    DLog(@"%@", dic);
    
//    NSString *jsonString;
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
//                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
//                                                         error:&error];
//    
//    if (! jsonData) {
//        NSLog(@"Got an error: %@", error);
//    } else {
//        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        DLog(@"%@",jsonString);
//    }
    
    
    
    Singleton *single = [Singleton retriveSingleton];
    
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    
    requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    requestManager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [requestManager.requestSerializer setValue:single.string_WCToken forHTTPHeaderField:@"WCToken"];
    [requestManager.requestSerializer setValue:single.string_WCTrustedToken forHTTPHeaderField:@"WCTrustedToken"];
    [requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [requestManager PUT:[NSString stringWithFormat:@"%@wcs/resources/store/10001/cart/@self/delete_order_item", k_url]
             parameters:dic
                success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
                    NSError *error = nil;
                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
                    
                    DLog(@"%@", json);
        
        if (json)
        {
            if ([json valueForKey:@"orderId"])
            {
                [self.delegate method_dataWithParams:nil dic:nil isSuccess:YES serviceName:k_webServiceRemoveCart];
//                [self.delegate method_data];
            }
            else
            {
                [self.delegate method_failureResponse];
            }
        }
        
        
    }
    
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    DLog(@"%@", error);
                    
                    [self.delegate method_failureResponse];
                }];

    
    
    
//    DLog(@"%@", dic);
//    
//
//    Singleton *single = [Singleton retriveSingleton];
//    
//    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
//    
//    
//    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
//    securityPolicy.validatesCertificateChain = NO;
//    
//    
//    // This usually would be a subclass of AFHTTPSessionManager
//    AFHTTPSessionManager *client = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:k_url] sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    client.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
//    client.securityPolicy.allowInvalidCertificates = YES;
//    
//    
////    requestManager.responseSerializer = [AFJSONResponseSerializer serializer];
////    requestManager.requestSerializer = [AFJSONRequestSerializer serializer];
//
//    
//    [requestManager.requestSerializer setValue:single.string_WCToken forHTTPHeaderField:k_WCToken];
//    [requestManager.requestSerializer setValue:single.string_WCTrustedToken forHTTPHeaderField:k_WCTrustedToken];
//    
////    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//    [requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    
//    [requestManager PUT:[NSString stringWithFormat:@"%@store/10001/cart/@self/delete_order_item", k_url]
//             parameters:dic
//                success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                    DLog(@"%@", responseObject);
//                }
//                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                    DLog(@"%@", error);
//                }];
    
    
    
//    [requestManager POST:[NSString stringWithFormat:@"%@wcs/resources/store/10001/guestidentity", k_url]
//              parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         DLog(@"%@", responseObject);
//     }
//                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                     
//                     DLog(@"%@", error);
//                     
//                     [[[UIAlertView alloc] initWithTitle:nil
//                                                 message:@"Authentication failed"
//                                                delegate:nil
//                                       cancelButtonTitle:@"Ok"
//                                       otherButtonTitles:nil, nil]
//                      show];
//                 }];
}
#pragma mark - method_loginidentity
-(void)method_loginidentity_serviceName:(NSString*)serviceName;                       // making the session with the server or we can say the guest user
{
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    securityPolicy.validatesCertificateChain = NO;
    
    // This usually would be a subclass of AFHTTPSessionManager
    AFHTTPSessionManager *client = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:k_url] sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    client.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    client.securityPolicy.allowInvalidCertificates = YES;
    
    
    requestManager.responseSerializer = [AFJSONResponseSerializer serializer];
    requestManager.requestSerializer = [AFJSONRequestSerializer serializer];
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:@"wcsadmin",@"logonId",@"passw0rd",@"logonPassword", nil];
    DLog(@"%@",parameters);
    
    [requestManager POST:[NSString stringWithFormat:@"%@wcs/resources/store/10001/loginidentity", k_url]
              parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         DLog(@"login identity:%@", responseObject);
         
         if ([responseObject valueForKey:k_WCToken])
         {
             [self.delegate method_dataWithParams:nil
                                              dic:responseObject
                                        isSuccess:YES
                                      serviceName:serviceName];
             
             
//             [self.delegate method_dataWithParams:nil
//                                              dic:responseObject
//                                      serviceName:serviceName];
         }
         else
         {
             DLog(@"Authentication failed for promotion code");
//             [[[UIAlertView alloc] initWithTitle:nil
//                                         message:@"Authentication failed for promotion code"
//                                        delegate:nil
//                               cancelButtonTitle:@"Ok"
//                               otherButtonTitles:nil, nil]
//              show];
         }
     }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     
                     DLog(@"%@", error);
                     DLog(@"Authentication failed for promotion code");
//                     [[[UIAlertView alloc] initWithTitle:nil
//                                                 message:@"Authentication failed for promotion code"
//                                                delegate:nil
//                                       cancelButtonTitle:@"Ok"
//                                       otherButtonTitles:nil, nil]
//                      show];
                 }];
}

#pragma mark - method_getPromotionCode
-(void)method_getPromotionCode:(NSDictionary *)dic serviceName:(NSString*)serviceName
{
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    requestManager.requestSerializer = [AFJSONRequestSerializer serializer];
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [requestManager.requestSerializer setValue:[dic valueForKey:@"WCToken"] forHTTPHeaderField:@"WCToken"];
    [requestManager.requestSerializer setValue:[dic valueForKey:@"WCTrustedToken"] forHTTPHeaderField:@"WCTrustedToken"];
    [requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    __block NSMutableArray *array_promotioCode = [[NSMutableArray alloc] init];
    [requestManager GET:@"https://arandell.royalcyber.com/wcs/resources/store/10001/promotion/10000002" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
         
         if (error != nil)
         {
             DLog(@"json: %@", json);
         }
         else
         {
             if ([json valueForKey:k_Promotion])
             {
                 [(NSArray *)[json valueForKey:k_Promotion] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
                 {
                     [array_promotioCode addObject:[NSDictionary dictionaryWithObjectsAndKeys:[obj valueForKey:@"promotionName"],@"promotionName",[obj valueForKey:@"resourceId"],@"resourceId", nil]];
                 }];
                 
                 [self.delegate method_dataWithParams:array_promotioCode
                                                  dic:nil
                                            isSuccess:YES
                                          serviceName:nil];
                 
//                 [self.delegate method_dataWithParams:array_promotioCode
//                                                  dic:nil
//                                          serviceName:nil];
                 
                 array_promotioCode = nil;
             }
             else
             {
                 DLog(@"promotion code not found");
             }
         }
     }
                failure:^(AFHTTPRequestOperation *operation, NSError *error)
            {
                DLog(@"promotion code not found");
                    DLog(@"%@", error);
                }];
}


#pragma mark - method_getProductImageBySKU
-(void)method_getProductImageBySKU:(NSString*)id_ serviceName:(NSString*)serviceName
{
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", nil];
    requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/productview/byId/%@", k_url, id_];//14451//13914
    
    DLog(@"%@", strUrl);
    
    
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [requestManager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
         
         if (error != nil)
         {
             NSLog(@"json: %@", json);
             [[[UIAlertView alloc] initWithTitle:nil
                                         message:@"There is some issue from server side, please try later"
                                        delegate:nil
                               cancelButtonTitle:@"Ok"
                               otherButtonTitles:nil, nil]
              show];
         }
         else
         {
             NSLog(@"Array: %@", json);
             
             [self.delegate method_dataWithParams:nil
                                              dic:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           [NSString stringWithFormat:@"%@%@",k_url,
                                                            [[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@k_detailImage]], @k_detailImage, nil]
                                        isSuccess:YES
                                      serviceName:serviceName];
             
             
//             [self.delegate method_dataWithParams:nil dic:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                           [NSString stringWithFormat:@"%@%@",k_url,[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@k_detailImage]], @k_detailImage, nil] serviceName:k_webServiceProductImage];
         }
     }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    NSLog(@"json: %@", error);
                    [[[UIAlertView alloc] initWithTitle:nil
                                                message:@"There is some issue from server side, please try later"
                                               delegate:nil 
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil, nil]
                     show];
                }];

}

@end