#import "SubmitFormViewController.h"
#import "DemoTableController.h"
#import "FPPopoverController.h"
#import "Singleton.h"
#import "CDatePickerViewEx.h"
#import "CardIO.h"
#import "GetProductDataFromServer.h"

#define SCROLLVIEW_HEIGHT 460
#define SCROLLVIEW_WIDTH  320

#define SCROLLVIEW_CONTENT_HEIGHT 720
#define SCROLLVIEW_CONTENT_WIDTH  320


#define kOFFSET_FOR_KEYBOARD 100.0


@interface SubmitFormViewController ()<UITextFieldDelegate,UIAlertViewDelegate,SelectedDate,UIPickerViewDataSource,UIPopoverControllerDelegate,CardIOPaymentViewControllerDelegate,DemoTableControllerDelegate,FPPopoverControllerDelegate,GetDateFromServer>
{
    BOOL           keyboardVisible;
    CGPoint        offset;
    
    Singleton *single;
    NSArray *array_cardType;
    FPPopoverController *popover;
    DemoTableController *controller;
}


@property (weak, nonatomic) IBOutlet UITextField *textField_name;
@property (weak, nonatomic) IBOutlet UITextField *textField_email;
@property (weak, nonatomic) IBOutlet UITextField *textField_contact;
@property (weak, nonatomic) IBOutlet UITextField *textField_address;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UITextField *textField_lastName;

@property (weak, nonatomic) IBOutlet UITextField *textField_creditCardType;
@property (weak, nonatomic) IBOutlet UITextField *textField_creditCardNo;
@property (weak, nonatomic) IBOutlet UITextField *textField_experitationDate;
@property (weak, nonatomic) IBOutlet UITextField *textField_securityCode;
@property(nonatomic, strong)UIPickerView *pickerView;
@property(nonatomic, strong)GetProductDataFromServer *obj_GetProductDataFromServer;

- (IBAction)action_submit:(id)sender;
- (IBAction)action_CreditCardScan:(id)sender;

@end

@implementation SubmitFormViewController



#pragma mark - cycle
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [CardIOUtilities preload];
    
    
    
//    // register for keyboard notifications
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
    
    
}

#pragma mark - action_goBack
-(void)action_goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    
    single = [Singleton retriveSingleton];
    
    [self method_addPickerInKeyBoard];
    
    [self.pickerView selectRow:0 inComponent:0 animated:NO];
    
    [self.scroller setContentSize:CGSizeMake(0, self.textField_securityCode.frame.origin.y+100)];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@k_backButtonImage]
                                                                             style:UIBarButtonItemStyleDone
                                                                            target:self
                                                                            action:@selector(action_goBack)];
    self.navigationItem.hidesBackButton = YES;
    
    
    if (popover == nil)
    {
        controller = [[DemoTableController alloc] init];
        controller.delegated = self;
        controller.title = nil;
        popover = [[FPPopoverController alloc] initWithViewController:controller];
        popover.delegate = self;
        popover.border = YES;
        popover.arrowDirection = FPPopoverArrowDirectionDown;
        popover.contentSize = CGSizeMake(270,270);
    }
    
    
    if (!self.obj_GetProductDataFromServer)
    {
        self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
        self.obj_GetProductDataFromServer.delegate = self;
    }
    
    
    
    
    self.scroller.frame = self.view.frame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    self.textField_address.delegate = nil;
    self.textField_contact.delegate = nil;
    self.textField_email.delegate = nil;
    self.textField_name.delegate = nil;
    self.scroller.delegate = nil;
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//    
//    // unregister for keyboard notifications while not visible.
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillShowNotification
//                                                  object:nil];
//    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillHideNotification
//                                                  object:nil];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}


//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}






#pragma mark - text field delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
            return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)sender
{
//    //move the main view, so that the keyboard does not hide it.
//    if  (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
    if (sender == self.textField_address)
    {
        
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.textField_name)
    {
        [self.textField_lastName becomeFirstResponder];
        return NO;
    }
    
    if (textField == self.textField_lastName)
    {
        [self.textField_email becomeFirstResponder];
        return NO;
    }
    
    
    if (textField == self.textField_email)
    {
        [self.textField_contact becomeFirstResponder];
        return NO;
    }
    if (textField == self.textField_contact)
    {
        [self.textField_address becomeFirstResponder];
        return NO;
    }
    
    
    if (textField == self.textField_address)
    {
        [self.textField_creditCardType becomeFirstResponder];
        return NO;
    }
    
    if (textField == self.textField_creditCardType)
    {
        [self.textField_creditCardNo becomeFirstResponder];
        return NO;
    }
    if (textField == self.textField_creditCardNo)
    {
        [self.textField_experitationDate becomeFirstResponder];
        return NO;
    }
    if (textField == self.textField_experitationDate)
    {
        [self.textField_securityCode becomeFirstResponder];
        return NO;
    }
    
    [self.textField_securityCode resignFirstResponder];
    return YES;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.textField_creditCardType ||
        textField == self.textField_creditCardNo ||
        textField == self.textField_securityCode ||
        textField == self.textField_experitationDate)
    {
        if ([self.textField_creditCardType.text length])
        {
            if (textField == self.textField_creditCardNo)
            {
                if ([self.textField_creditCardType.text isEqualToString:@"MasterCard"]||[self.textField_creditCardType.text isEqualToString:@"Visa"])
                {
                    __block NSString *text = [textField text];
                    
                    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
                    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
                    if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
                        return NO;
                    }
                    
                    text = [text stringByReplacingCharactersInRange:range withString:string];
                    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    NSString *newString = @"";
                    while (text.length > 0) {
                        NSString *subString = [text substringToIndex:MIN(text.length, 4)];
                        newString = [newString stringByAppendingString:subString];
                        if (subString.length == 4) {
                            newString = [newString stringByAppendingString:@" "];
                        }
                        text = [text substringFromIndex:MIN(text.length, 4)];
                    }
                    
                    newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
                    
                    if (newString.length >= 20) {
                        return NO;
                    }
                    
                    [textField setText:newString];
                    
                    return NO;
                }
                
                if([self.textField_creditCardType.text isEqualToString:@"American Express"])
                {
                    if (range.location<=16) {
                        
                        if (range.location==4)
                        {
                            NSMutableString *Text = nil;
                            Text = [NSMutableString stringWithFormat:@"%@",textField.text];
                            [Text insertString:@" " atIndex:4];
                            [textField setText:Text];
                        }
                        
                        if (range.location==11)
                        {
                            NSMutableString *Text = nil;
                            Text = [NSMutableString stringWithFormat:@"%@",textField.text];
                            [Text insertString:@" " atIndex:11];
                            [textField setText:Text];
                        }
                        return YES;
                    }
                    return NO;
                }
                return NO;
            }
            
            if (self.textField_securityCode == textField)
            {
                if ([self.textField_creditCardType.text isEqualToString:@"MasterCard"]||[self.textField_creditCardType.text isEqualToString:@"Visa"])
                {
                    if ([textField.text length] > 2 && ![string isEqualToString:@""])
                    {
                        return NO;
                    }
                }
                else
                {
                    if ([textField.text length] > 3 && ![string isEqualToString:@""])
                    {
                        return NO;
                    }
                }
                return YES;
                
            }
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:nil message:@"Select card type first"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
            return NO;
        }
    }
    else if (textField == self.textField_address)
    {
        NSString *text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString: string];
        if ([text isEqualToString:@""] && popover != nil)
        {
            [popover dismissPopoverAnimated:YES];
        }
        else
        {
            [self.obj_GetProductDataFromServer method_autocompleteAddress_input:text];
        }
    }
    return YES;
}


#pragma mark - method_submitForm
- (void)method_submitForm
{
    [[[UIAlertView alloc] initWithTitle:nil
                                message:@"Order successfully placed"
                               delegate:nil
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles:nil, nil]
     show];
    
    
//    DLog(@"%@", [NSString stringWithFormat:@"http://192.168.16.48/CaptureLife/CaptureLifeFeedback/?method=feedback&format=json&name=%@&email=%@&address=%@contact=%@&productId=%@&productName=%@",
//                 self.textField_name.text,
//                 self.textField_email.text,
//                 self.textField_address.text,
//                 self.textField_contact.text,
//                 single.productDetail.p_id,
//                 single.productDetail.p_name]);
//    
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.16.48/CaptureLife/CaptureLifeFeedback/?method=feedback&format=json&name=%@&email=%@&address=%@&contact=%@&productId=%@&productName=%@",
//                                       self.textField_name.text,
//                                       self.textField_email.text,
//                                       self.textField_address.text,
//                                       self.textField_contact.text,
//                                       single.productDetail.p_id,
//                                       single.productDetail.p_name]];

    
    
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    [NSURLConnection sendAsynchronousRequest:request
//                                       queue:[NSOperationQueue mainQueue]
//                           completionHandler:^(NSURLResponse *response,
//                                               NSData *data, NSError *connectionError)
//     {
//         if (data.length > 0 && connectionError == nil)
//         {
//             NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data
//                                                                          options:0
//                                                                            error:NULL];
//
//             DLog(@"responseData: %@", responseData);
//
//             if ([responseData isKindOfClass:[NSNull class]] || responseData == nil)
//             {
//                 [[[UIAlertView alloc] initWithTitle:@"Sorry"
//                                             message:@"There is something is wrong from server side, please try later"
//                                            delegate:nil
//                                   cancelButtonTitle:@"Ok"
//                                   otherButtonTitles:nil, nil]
//                  show];
//             }
//             else
//             {
//                 [[[UIAlertView alloc] initWithTitle:@"Status"
//                                             message:[[responseData valueForKey:@"data"] valueForKey:@"orderStatus"]
//                                            delegate:nil
//                                   cancelButtonTitle:@"Ok"
//                                   otherButtonTitles:nil, nil]
//                  show];
//                 
//                 
////                 [16/01/2015 5:55:56 pm] Talha Haroon: {"code":0,"status":404,"data":{"orderStatus":"Successfully Completed","orderStatusCode":1}}
////                 [16/01/2015 5:56:09 pm] Talha Haroon: orderstatusCode: 0 hojaega unsuccessful hua to
//             }
//         }
//         else
//         {
//             [[[UIAlertView alloc] initWithTitle:@"Sorry"
//                                         message:@"There is something is wrong from server side, please try later"
//                                        delegate:nil
//                               cancelButtonTitle:@"Ok"
//                               otherButtonTitles:nil, nil]
//              show];
//         }
//     }];
}

#pragma mark - action_submit
- (IBAction)action_submit:(id)sender
{
        if (![self.textField_name.text length])
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Please enter your First Name"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
        else if (![self.textField_lastName.text length])
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Please enter your Last Name"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
             show];
        }
        else if (![self.textField_email.text length] || [single method_NSStringIsValidEmail:self.textField_email.text] == false)
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Invalid email address"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
        else if ([self.textField_contact.text length] && [single method_phoneNumberValidation:self.textField_contact.text] == false)
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Invalid Contact Number"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
        else if (![self.textField_address.text length])
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Enter your Address"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
        else if (![self.textField_creditCardNo.text length])
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Enter your Credit Card Number"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
        else if (![self.textField_creditCardType.text length])
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Enter your Credit Card Type"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
        else if (![self.textField_experitationDate.text length])
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Enter your Credit Card Expiration date"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
        else if (![self.textField_securityCode.text length])
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Enter your Credit Card Security Code"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
        else
        {
    
            //[self method_submitForm];
           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                        message:@"Order submitted successfully"
                                       delegate:self
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil];
            alert.tag = 100;
            [alert show];
        }
}
#pragma mark - action_CreditCardScan
- (IBAction)action_CreditCardScan:(id)sender
{
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    [self presentViewController:scanViewController animated:YES completion:nil];
}

#pragma mark - add picker in text keyBoard
-(void)method_addPickerInKeyBoard
{
    array_cardType = [NSArray arrayWithObjects:@"MasterCard",@"Visa", @"American Express", nil];
    
    
    self.pickerView = [[UIPickerView alloc] init];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    // ... ...
    self.textField_creditCardType.inputView = self.pickerView;
    
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                        target:nil
                                                                        action:nil],
                           
                           [[UIBarButtonItem alloc]initWithTitle:@"Next"
                                                           style:UIBarButtonItemStyleDone
                                                          target:self
                                                          action:@selector(action_done)], nil];
    
    [numberToolbar sizeToFit];
    self.textField_creditCardType.inputAccessoryView = numberToolbar;
    
    numberToolbar = nil;
    
    CDatePickerViewEx *datePicker = [[CDatePickerViewEx alloc]init];
    datePicker.delegate_mine = self;
    [self.textField_experitationDate setInputView:datePicker];
}
#pragma mark - updateExpiryDateTextField
-(void)updateExpiryDateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.textField_experitationDate.inputView;
    NSDate * selected = [picker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM/yy"];
    self.textField_experitationDate.text = [dateFormatter stringFromDate:selected];
}
#pragma mark - bar button
-(void)action_done
{
    self.textField_creditCardNo.text = @"";
    self.textField_securityCode.text = @"";
    
    
    [self.textField_creditCardType resignFirstResponder];
    
    [self.textField_creditCardNo becomeFirstResponder];
    
    self.textField_creditCardType.text = [array_cardType objectAtIndex:[self.pickerView selectedRowInComponent:0]];
    
    
    if ([self.textField_creditCardType.text isEqualToString:@"Visa"])
    {
        // typerang = 2;
        
        //[self.imageView_CardType setImage:[UIImage imageNamed:@"visa"]];
        
        [self.textField_creditCardNo setPlaceholder:@"0000 0000 0000 0000"];
        [self.textField_securityCode setPlaceholder:@"000"];
    }
    else if ([self.textField_creditCardType.text isEqualToString:@"MasterCard"])
    {
        //  typerang = 2;
        
       // [self.imageView_CardType setImage:[UIImage imageNamed:@"mastercard"]];
        
        [self.textField_creditCardNo setPlaceholder:@"0000 0000 0000 0000"];
        [self.textField_securityCode setPlaceholder:@"000"];
    }
    else if ([self.textField_creditCardType.text isEqualToString:@"American Express"])
    {
        //  typerang = 3;
        
       // [self.imageView_CardType setImage:[UIImage imageNamed:@"American_Express"]];
        
        [self.textField_creditCardNo setPlaceholder:@"0000 000000 00000"];
        [self.textField_securityCode setPlaceholder:@"0000"];
    }
}
#pragma mark - method_textFieldIsEmpty
-(BOOL)method_textFieldIsEmpty:(UITextField *)textField_
{
    NSString *rawString = [textField_ text];
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0)
    {
        // Text was empty or only whitespace.
        return YES;
    }
    return NO;
}
#pragma mark - uialertview delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag] == 100)
    {
        if (buttonIndex == 0)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - picker delegate and data source
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isKindOfClass:[CDatePickerViewEx class]])
    {
        return 0;
    }
    return [array_cardType count]?[array_cardType count]:0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [array_cardType objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}
#pragma mark - SelectedDate delegate
-(void)method_selectedDate:(NSString *)string_date
{
    self.textField_experitationDate.text = string_date;
}




#pragma mark - PaymentViewControllerDelegate
- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
    NSLog(@"User canceled payment info");

    // Handle user cancellation here...
    [scanViewController dismissViewControllerAnimated:YES completion:^{
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
    // The full card number is available as info.cardNumber, but don't log that!
    NSLog(@"Received card info. Number: %@, expiry: %02i/%i, cvv: %@.", info.redactedCardNumber, info.expiryMonth, info.expiryYear, info.cvv);
    NSLog(@"card type:%@",[CardIOCreditCardInfo displayStringForCardType:info.cardType usingLanguageOrLocale:@""]);
    // Use the card info...
    
    self.textField_creditCardNo.text = info.cardNumber;
    self.textField_creditCardType.text = [CardIOCreditCardInfo displayStringForCardType:info.cardType usingLanguageOrLocale:@""];
    self.textField_securityCode.text = info.cvv;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setMonth:info.expiryMonth];
    [components setYear:info.expiryYear];
    NSDate * selected = [calendar dateFromComponents:components];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MMMM/yyyy"];
    self.textField_experitationDate.text = [dateFormatter stringFromDate:selected];
    components = nil;
    dateFormatter = nil;
//    self.textField_experitationDate.text = [NSString stringWithFormat:@"%lu/%lu",(unsigned long)info.expiryMonth,(unsigned long)info.expiryYear];
//    NSLog(@"%@", info.postalCode);
//    NSLog(@"%@", info.cvv);
//    NSLog(@"%lu", (unsigned long)info.expiryYear);
//    NSLog(@"%lu", (unsigned long)info.expiryMonth);
//    NSLog(@"%@", info.cardNumber);
//    NSLog(@"%ld", info.cardType);
//    NSLog(@"%@", info.cardImage);
    
    [scanViewController dismissViewControllerAnimated:YES completion:^{
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }];
}
#pragma mark - Popup delegate
#pragma mark - SelectPopValue
- (void)selectedValueFromDropDown:(NSString *)selectedValue controller:(DemoTableController *)controller
{
    self.textField_address.text = selectedValue;
    [popover dismissPopoverAnimated:YES];
}
- (void)presentedNewPopoverController:(FPPopoverController *)newPopoverController
          shouldDismissVisiblePopover:(FPPopoverController*)visiblePopoverController
{
    [visiblePopoverController dismissPopoverAnimated:YES];
}
- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController
{
//    popover = nil;
}



#pragma mark - webservice response
-(void)method_dataWithParams:(NSArray *)array_ dic:(NSDictionary *)dic_ isSuccess:(BOOL)isSuccess serviceName:(NSString *)serviceName
{
    if ([array_ count])
    {
        NSLog(@"%@",array_);
        [controller reloadList:array_];
        
        UIScrollView *v = (UIScrollView *) self.view;
        CGRect rc = [self.textField_address bounds];
        rc = [self.textField_address convertRect:rc toView:v];
        
        DLog(@"%f", rc.origin.y);
        
        if (rc.origin.y <= 145)     // it means textfield is near to topBar
        {
            popover.arrowDirection = FPPopoverArrowDirectionUp;
        }
        else
        {
            popover.arrowDirection = FPPopoverArrowDirectionDown;
        }
        
        
        [popover presentPopoverFromView:self.textField_address];
    }
    else
    {
        [popover dismissPopoverAnimated:YES];
    }
}

-(void)method_failureResponse
{
    NSLog(@"failure response");
}

@end