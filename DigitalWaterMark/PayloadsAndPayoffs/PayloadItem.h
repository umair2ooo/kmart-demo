//
//  PayloadItem.h
//  Digimarc Mobile SDK: DMSDemo
//
//  Application level capture of payload attributes, suitable for driving the application payloads list, and detail views.
//
//  Created by localTstewart on 7/31/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMPayload.h"

@interface PayloadItem : NSObject

@property (retain) NSDate* reportedDate;
@property int itemNumber;
@property (retain) DMPayload* payload;
@property (retain) NSDictionary* readerInfo;

@end
