//
//  PayoffDoc.h
//  DMSDemo
//
//  Created by Sinclair, Eoin on 3/1/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PayoffItem.h"

@interface PayoffDoc : NSObject

@property (copy)                NSString *docPath;
@property (nonatomic, retain)   PayoffItem *payoffItem;

- (id)init;
- (id)initWithDocPath:(NSString *)docPath;
- (void)saveData;
- (void)deleteDoc;

@end
