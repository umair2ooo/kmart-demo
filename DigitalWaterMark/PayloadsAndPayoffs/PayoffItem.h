//
//  PayoffItem.h
//  DMSDemo
//
//  Created by Sinclair, Eoin on 2/13/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMResolver.h"

#define kImageDataKey       @"DMSDemo_ImageData"
#define kDateKey            @"DMSDemo_Date"
#define kPayloadKey         @"DMSDemo_ResolveResult"
#define kTitleKey           @"DMSDemo_Title"
#define kSubtitleKey        @"DMSDemo_Subtitle"
#define kTitleKey           @"DMSDemo_Title"
#define kTokenKey           @"DMSDemo_ActionToken"
#define kContentKey         @"DMSDemo_Content"

@interface PayoffItem : NSObject <NSCoding>

@property (nonatomic, retain) NSData            *imageData;
@property (nonatomic, retain) NSDate            *date;
@property (nonatomic, retain) DMPayload         *payload;
@property (nonatomic, retain) NSString          *title;
@property (nonatomic, retain) NSString          *subtitle;
@property (nonatomic, retain) NSString          *actionToken;
@property (nonatomic, retain) NSString          *content;

@end
