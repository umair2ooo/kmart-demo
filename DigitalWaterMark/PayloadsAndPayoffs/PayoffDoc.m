//
//  PayoffDoc.m
//  DMSDemo
//
//  Created by Sinclair, Eoin on 3/1/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import "PayoffDoc.h"
#import "PayoffDatabase.h"
#define kDataKey        @"DMSDemo_DocData"
#define kDataFile       @"data.plist"

@implementation PayoffDoc

- (id)init {
    return ([self initWithDocPath:nil]);
}

- (id)initWithDocPath:(NSString *)docPath {
    if (self = [super init]) {
        _payoffItem = [[PayoffItem alloc] init];
        _docPath = [docPath copy];
    }
    return self;
}

- (BOOL)createDataPath {
    if (!_docPath)
        self.docPath = [PayoffDatabase nextPayoffDocPath];
    NSError *error;
    BOOL success = [[NSFileManager defaultManager] createDirectoryAtPath:_docPath withIntermediateDirectories:YES attributes:nil error:&error];
    if (!success)
        NSLog(@"Error creating data path: %@", [error localizedDescription]);
    return success;
}

- (PayoffItem *)payoffItem {
    if (_payoffItem.payload)
        return _payoffItem;
    
    [self createDataPath];
    
    NSString *dataPath = [_docPath stringByAppendingPathComponent:kDataFile];
    NSData *codedData = [[NSData alloc] initWithContentsOfFile:dataPath];
    if (!codedData)
        _payoffItem = [[PayoffItem alloc] init];
    else {
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:codedData];
        _payoffItem = [unarchiver decodeObjectForKey:kDataKey];
        [unarchiver finishDecoding];
    }
    if (!_payoffItem)
        _payoffItem = [[PayoffItem alloc] init];
    
    return _payoffItem;
}

- (void)saveData {
    if (!_payoffItem)
        return;
    
    [self createDataPath];
    NSString *dataPath = [_docPath stringByAppendingPathComponent:kDataFile];
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:_payoffItem forKey:kDataKey];
    [archiver finishEncoding];
    [data writeToFile:dataPath atomically:NO];
}

- (void)deleteDoc {
    NSError *error;
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:_docPath error:&error];
    if (!success)
        NSLog(@"Error removing document path: %@", error.localizedDescription);
}

-(void)dealloc {
    _docPath = nil;
}

@end
