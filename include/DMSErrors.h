//
//  DMSErrors.h
//  Digimarc Mobile SDK (DMS)
//
//  Created by localTstewart on 12/3/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#ifndef DMSErrors_h
#define DMSErrors_h


#define DMSErrorDomain                                           @"DigimarcMobileSDK"
#define DMSErrorUnsupportedAudioSampleRateCode                   1
#define DMSErrorUnsupportedAudioSampleRate                       @"Audio initiailization failed, unsupported sample rate"
#define DMSErrorUnsupportedAudioBitsPerSampleCode                2
#define DMSErrorUnsupportedAudioBitsPerSample                    @"Audio initialization failed, unsupported bits per sample"

#define DMSWarningSource                                         @"DMSManager"
#define DMSWarning_ImageMediaChangedCode                         1
// string with format: @"imageMediaChanged: %@", with detail percolating up from image source module
#define DMSWarning_AudioMediaChangedCode                         2
// string with format: @"audioMediaChanged: %@", with detail percolating up from audio source module

#define DMSWarningAudioReaderMgr                                 @"DMSAudioReaderMgr"
#define DMSWarningAudioReaderMgr_JSONParsingExceptionCode        1
#define DMSWarningAudioReaderMgr_JSONParsingException            @"Caught exception while building audio readers from JSON config"
#define DMSWarningAudioReaderMgr_FailedReaderConfigCode          2
// string with format: @"failed to add audio reader:%@" with name of failed audio reader section in JSON config file

#define DMSWarningSampleImageSource                              @"DMSImageCameraSource"
#define DMSWarningSampleImageSource_AVCaptureInputFailedCode     1
#define DMSWarningSampleImageSource_AVCaptureInputFailed         @"AVCaptureDeviceInput failed"
#define DMSWarningSampleImageSource_AVCaptureOutputFailedCode    2
#define DMSWarningSampleImageSource_AVCaptureOutputFailed        @"AVCaptureDeviceOutput failed"
#define DMSWarningSampleImageSource_AVCaptureSessionErrorCode    3
// string with format: @"AVCaptureSessionRuntimeErrorNotification: %@: %@", error.localizedDescription, error.localizedFailureReason
#define DMSWarningSampleImageSource_AVCaptureRestartedCode       4
#define DMSWarningSampleImageSource_AVCaptureRestarted           @"Apple was right.  Needed to tear down AVCapture objects and restart."

#endif
