//
//  DMSDelegateProtocol.h
//  Digimarc Mobile SDK (DMSDK)
//
//  Created by localTstewart on 7/18/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DMSManager;
@class DMPayload;   

/*--------------------------------------------------------------------------------------*/
/** @protocol DMSDelegateProtocol
* See the @ref DMSDelegateGroup "DMSDelegateProtocol API" for complete documentation of this protocol.
* 
*/
/** @defgroup DMSDelegateGroup DMSDK Delegate Protocol
* The DMSDelegateProtocol defines the interface for receiving notifications of watermark and barcode detections, status changes, and error events. 
*
* The protocol is typically implemented by the application model or view controller
*/


/** @defgroup statusNotifications DMSManager reporting of status changes, errors, or warnings.
* Notification of status changes, errors and warnings from the DMSManager
* @ingroup DMSDelegateGroup
*/
#define DMSErrorDomain @"DigimarcMobileSDK"							///< @ingroup statusNotifications
#define DMSErrorUnsupportedAudioSampleRateCode           1			///< @ingroup statusNotifications
#define DMSErrorUnsupportedAudioBitsPerSampleCode        2			///< @ingroup statusNotifications

#define DMSErrorUnsupportedAudioSampleRate           @"Audio initiailization failed, unsupported sample rate"       ///< @ingroup statusNotifications
#define DMSErrorUnsupportedAudioBitsPerSample        @"Audio initialization failed, unsupported bits per sample"    ///< @ingroup statusNotifications

/** Indicates current status of DMSManager
* @ingroup statusNotifications
*/
typedef enum DMSStatus : NSUInteger
{
    DMSStatusClosed,			///< DMSManager is closed.
    DMSStatusOpen				///< DMSManager is open.
} DMSStatus;


@protocol DMSDelegateProtocol <NSObject>
@optional

/**  Notification of change in DMSManager status
* @param dms Reference to DMSManager singleton
* @param status New DMSManager status
* @ingroup statusNotifications
*/
- (void) dms:(DMSManager*)dms statusChanged:(DMSStatus)status;


/** A fatal, unrecoverable error has occurred within the DMSManager, readers, or media sources.
*
* By convention, DMSManager reports transient errors, media faults, media format complaints,
* and other issues as warnings, not errors.  Error reporting is reserved for significant
* faults that indicate continued operation of the current instance of DMSManager is unlikely
* to recover.
*
* @param dms reference to the DMSManager singleton
* @param error NSError object.  Some errors are classified as DMSManager or DMResolver error domains, others are passed through with original error notifications from the underlying system frameworks.
* @ingroup statusNotifications
*/
- (void) dms:(DMSManager*)dms reportedError:(NSError*)error;


/** A problem was reported within the DMSManager, readers or media sources.
*
* By convention, fatal errors are reported with dms:reportedError: notification (see above).
* All other unexpected events are reported as warnings using this API.
*
* Applications should consider logging both errors and warnings to the system console.
* Warnings probably do not need to otherwise interrupt the UI, and are usually recoverable
* with continuing media source input.  In the event of failure to detect payloads, or
* otherwise poor performance, application developer should check the system console for
* clues reported as these warning messages.
*
* @param dms Reference to the DMSManager singleton
* @param warningSource Name of reporting module within DMSDK
* @param msg Warning message itself.  These messages are highly variable in format, and
* are intended for a technical audience.  They are typically appropriate for logging, but
* not designed for direct display on the application UI.
* @ingroup statusNotifications
*/
- (void) dms:(DMSManager*)dms reportedWarningFrom:(NSString*)warningSource message:(NSString*)msg;	///< @ingroup errorNotification


/** @defgroup detectionNotification DMSManager reporting of new or duplicate payloads detected
* 
* All payload detections are reported, including duplicate detection of the same or recent events.
* The <code>isNew</code> flag indicates notification of a payload not already in the DMSManager imagePayloadCache or audioPayloadCache.
*
* The application may optionally configure DMSManager to set cache depth to 0 (disabled), 1 (notify on any change from most recently reported payload, 
* including reporting concurrent/alternating payloads on each detection), or more.  Default cache depth is 1, so that a series of reads of the same payload generates a notification only on the first read. Another new notification occurs only when the payload changes or the cache is cleared.
*
* The application may also optionally set a DMSManager property to be notified of only new detections.  In this case any notifications with
* the <code>isNew</code> parameter equal to <code>false</code> will be suppressed and will not be relayed to the DMSDelegate.
*
* The optional readerInfo dictionary associated with each notification may be nil, or may contain reader-specific dictionary 
* of context information related to the detection.  Dictionary contents may be inspected by logging the entire dictionary object.
* Current DMSDK readers provide:
*	- Digimarc Image Watermarks: detailed info.  See \#define keys in DMReader.h interface.
*	- Digimarc Audio Watermarks: currently return no readerInfo (nil).
*	- Barcodes: return simple info containing only the barcode value, Digimarc CPM path string, and whether or not the barcode value can be directly used as a URL.
*
* The application may optionally suppress receiving any readerInfo by setting a DMSManager property.  Disabling readerInfo reporting
* reduces memory footprint, and marginally improves performance.
*
* @ingroup DMSDelegateGroup
*/


/** Notify a Digimarc image watermark detected
* @param dms Reference to the DMSManager
* @param payload DMPayload containing the watermark
* @param readerInfo NSDictionary of optional reader specific diagnostic info
* @param isNew BOOL YES if this payload not previously in the DMSManager imagePayloadCache
* @ingroup detectionNotification
*/
- (void) dms:(DMSManager*)dms detectedImageWatermark:(DMPayload*)payload withReaderInfo:(NSDictionary*)readerInfo isNew:(BOOL)isNew;


/** Notify a Digimarc audio watermark detected
* @param dms Reference to the DMSManager
* @param payload DMPayload containing the watermark
* @param readerInfo nil
* @param isNew BOOL YES if this payload not previously in the DMSManager audioPayloadCache
* @ingroup detectionNotification
*/
- (void) dms:(DMSManager*)dms detectedAudioWatermark:(DMPayload*)payload withReaderInfo:(NSDictionary*)readerInfo isNew:(BOOL)isNew;


/** Notify a 1D Barcode detected
* @param dms Reference to the DMSManager
* @param payload DMPayload containing the barcode info
* @param readerInfo NSDictionary of optional reader specific diagnostic info
* @param isNew BOOL YES if this payload not previously in the DMSManager imagePayloadCache
* @ingroup detectionNotification
*/
- (void) dms:(DMSManager*)dms detectedBarcode:(DMPayload*)payload withReaderInfo:(NSDictionary*)readerInfo isNew:(BOOL)isNew;


/** Notify a QRCode Barcode detected
* @param dms Reference to the DMSManager
* @param payload DMPayload containing the watermark
* @param readerInfo NSDictionary of optional reader specific diagnostic info.  This info will indicate whether the payload value can be used directly as a URL.
* @param isNew BOOL YES if this payload not previously in the DMSManager imagePayloadCache
* @ingroup detectionNotification
*/
- (void) dms:(DMSManager*)dms detectedQRcode:(DMPayload*)payload withReaderInfo:(NSDictionary*)readerInfo isNew:(BOOL)isNew;

@end

/*--------------------------------------------------------------------------------------*/
/** @protocol DMSImageSourceProtocol
* See the @ref DMSImageSourceProtocolGroup "DMSImageSourceProtocol API" for complete documentation of this protocol.
*/
/** @defgroup DMSImageSourceProtocolGroup DMSDK Image Source Protocol
* The DMSImageSourceProtocol defines the interface for associating image media sources with the DMSDK and enabling control of the media sources by the DMSDK.
*
* DMSImageSourceProtocol is typically implemented by a camera or image file utility. Sample implementations are provided by the SDK, but the app may fully
* customize its own media handlers so long as they conform to this protocol.
*
* All DMSImageSourceProtocol methods are synchronous -- the image source must complete all related work before returning to DMSManager.
*
* If the image source will be using the camera, the [DMSManager initializeCaptureSession:andDevice:] method should be used
* to select the best available camera format for use with DMSDK.  Once this call has been completed, the
* AVCaptureSession and/or AVCaptureDevice properties may be inspected for information on the selected format.
*
* Any runtime errors encountered in the image source should typically be reported via [DMSManager postWarningFrom:message:] method.  More severe
* errors that suggest image source is no longer operable within the current DMSManager session should be reported with [DMSManager postError:] method.
*/
@protocol DMSImageSourceProtocol <NSObject>
@required


/** Attach image source to DMSManager.
*
* This method is called during DMSManager processing of the openSessionWithImageSource:audioSource: method.
* It should not be called directly by the application or any other entities.
*
* The imageSource component on receiving this message should save a weak reference to the DMSManager,
* and prepare to respond to format attribute queries.
*
* @param dms Reference to the DMSManager singleton
* @ingroup DMSImageSourceProtocolGroup
*/
- (BOOL) attachToDms:(DMSManager*)dms;


/** Detach image source from DMSManager
*
* This method is called during DMSManager processing of the closeSession method.
* It should not be called directly by the application or any other entities.
*
* The imageSource component on receiving this message should stop any current
* streaming operations, clear any content buffers, and release the weak reference to DMSManager
* (replacing this value with nil).
*
* @param dms Reference to the DMSManager singleton
* @ingroup DMSImageSourceProtocolGroup
*/
- (void) detachFromDms:(DMSManager*)dms;


/** Start the flow of incoming image frames
*
* This method is called during DMSManager processing of the startImageSource method.
* It should not be called directly by the application or any other entities.
*
* The imageSource component on receiving this message should begin reading image data
* from camera, files, network stream, or other sources, and relaying image frame data 
* into DMSManager.
*
* @ingroup DMSImageSourceProtocolGroup
*/
- (BOOL) start;


/** Stop the flow of incoming image frames
*
* This method is called during DMSManager processing of the stopImageSource method.
* It should not be called directly by the application or any other entities.
*
* The imageSource component on receiving this message should stop reading image data
* from camera, files, network stream, or other sources, and stop relaying image frame
* data into DMSManager.
*
* The application may typically call startImageSource/stopImageSource repeatedly
* within the scope of the same DMSManager session.  This implements pause/resume
* behavior as the user navigates between different UI activities in the application,
* without fully shutting down the system.  The imageSource should be prepared to
* take whatever steps are appropriate for this situation.
*
* @ingroup DMSImageSourceProtocolGroup
*/
- (void) stop;


/**
* Controls whether DMSManager should process incoming media synchronously.
* 
* - For file based sources, this will typically be YES to provide easy source throttling of the media source to match the pace that the DMSDK can process incoming media.
* - For streaming image sources, ideally this would be NO.
*
* Current DMSDK 1.0 implementation forces this parameter to YES internally.  This is done
* to reduce memory footprint, heap churning, and heap fragmentation.  This can be a
* problem with very large image frames arriving rapidly, then being held in memory for
* arbitrary periods of time waiting for async processing.
*
* This query attribute remains in the imageSourceDelegate API to support future versions
* of DMSDK which may provide a more flexible processing dispatch.
* @ingroup DMSImageSourceProtocolGroup
*/
- (BOOL) dmsShouldPerformSynchronousReadsOnIncomingThread;


/** Current frame rate -- not used, reserved for future use.
* @ingroup DMSImageSourceProtocolGroup
*/
- (int) currentFrameRate;


/** Request frame rate -- not used, reserved for future use.
* @ingroup DMSImageSourceProtocolGroup
*/
- (int) requestFrameRate:(int)newValue;

@end

/*--------------------------------------------------------------------------------------*/
/** @protocol DMSAudioSourceProtocol
* See the @ref DMSAudioSourceProtocolGroup "DMSAudioSourceProtocol API" for complete documentation of this protocol.
* 
*/
/** @defgroup DMSAudioSourceProtocolGroup DMSDK Audio Source Protocol
* The DMSAudioSourceProtocol defines the interface for associating audio media sources with the DMSDK and enabling control of the media sources by the DMSDK.
*
* DMSAudioSourceProtocol is typically implemented by microphone capture or audio file playback utility. Sample implementations are provided by the SDK, but the app may fully customize its own media handlers so long as they conform to this protocol.
*
* All DMSAudioSourceProtocol methods are synchronous -- the audio source must complete all related work before returning to DMSManager.
*
* Any runtime errors encountered in the audio source should typically be reported via [DMSManager postWarningFrom:message:] method.  More severe
* errors that suggest audio source is no longer operable within the current DMSManager session should be reported with [DMSManager postError:] method.
*/
@protocol DMSAudioSourceProtocol <NSObject>
@required


/** Attach audio source to DMSManager.
*
* This method is called during DMSManager processing of the openSessionWithImageSource:audioSource: method.
* It should not be called directly by the application or any other entities.
*
* The audioSource component on receiving this message should save a weak reference to the DMSManager,
* and prepare to respond to format attribute queries.
*
* @param dms Reference to the DMSManager singleton
* @ingroup DMSAudioSourceProtocolGroup
*/
- (BOOL) attachToDms:(DMSManager*)dms;


/** Detach audio source from DMSManager
*
* This method is called during DMSManager processing of the closeSession method.
* It should not be called directly by the application or any other entities.
*
* The audioSource component on receiving this message should stop any current
* streaming operations, clear any content buffers, and release the weak reference to DMSManager
* (replacing this value with nil).
*
* @param dms Reference to the DMSManager singleton
* @ingroup DMSAudioSourceProtocolGroup
*/
- (void) detachFromDms:(DMSManager*)dms;


/** Start the flow of incoming audio sample buffers
*
* This method is called during DMSManager processing of the startAudioSource method.
* It should not be called directly by the application or any other entities.
*
* The audioSource component on receiving this message should begin reading audio data
* from microphone, files, network stream, or other sources, and relaying audio sample data 
* into DMSManager.
*
* @ingroup DMSAudioSourceProtocolGroup
*/
- (BOOL) start;


/** Stop the flow of incoming audio sample buffers
*
* This method is called during DMSManager processing of the stopAudioSource method.
* It should not be called directly by the application or any other entities.
*
* The audioSource component on receiving this message should stop reading audio data
* from microphone, files, network stream, or other sources, and stop relaying audio sample
* data into DMSManager.
*
* The application may typically call startAudioSource/stopAudioSource repeatedly
* within the scope of the same DMSManager session.  This implements pause/resume
* behavior as the user navigates between different UI activities in the application,
* without fully shutting down the system.  The audioSource should be prepared to
* take whatever steps are appropriate for this situation.
*
* The DMSManager itself implements audio buffering of incoming samples, accumulating
* and splitting the incoming sample blocks as necessary to build a sequence of
* contiguous 1/8 second frames.  It is these internally buffered frames that are
* dispatched to the audio readers.  When the audio system is stopped, DMSManager
* flushes any currently buffered content so that on next start there will not be
* a discontinuity in the data.
*
* @ingroup DMSAudioSourceProtocolGroup
*/
- (void) stop;


/** Should DMSManager process incoming audio samples synchronously?
*
* - For file-based audio sources, this should be YES.  This provides easy rate control,
* throttling the file reading thread to the rate that DMSManager can process the incoming
* data.
* 
* - For streaming audio sources, this will typically be NO.   The incoming audio is quickly buffered, 
* with immediate return to the audio source (typically called on the underlying media framework notification thread).
*
* @ingroup DMSAudioSourceProtocolGroup
*/
- (BOOL) dmsShouldPerformSynchronousReadsOnIncomingThread;


/** Query for sample rate of incoming audio data
*
* DMSManager will query this method during openSessionWithImageSource:audioSource:
* initialization.   It may also re-query this method if a media change event is detected,
* such as when switching between files in a list of audio titles.
*
* - DMSManager supports only 16KHz (16000 samples per second).
*
* If the source audio graph or media file is any other sample rate, it must first be converted
* to 16KHz before relaying into DMSManager.   This can be easily done using an AudioUnits
* processing graph with a format conversion module.
*
* @ingroup DMSAudioSourceProtocolGroup
*/
- (int) sampleRate;


/** Query for bits per sample format
*  
* DMSManager will query this method during openSessionWithImageSource:audioSource:
* initialization.   It may also re-query this method if a media change event is detected,
* such as when switching between files in a list of audio titles.
*
* - DMSManager supports only 16-bit (short) or 32-bit (float) PCM audio data.
*
* If the source audio graph or media file is any other format, it must first be converted
* to 16 or 32 bit PCM before relaying into DMSManager.   This can be easily done using an
* AudioUnits processing graph with a format conversion module.
*
* @ingroup DMSAudioSourceProtocolGroup
*/
- (int) bitsPerSample;


/** Query for number of audio channels in incoming samples data
*  
* DMSManager will query this method during openSessionWithImageSource:audioSource:
* initialization.   It may also re-query this method if a media change event is detected,
* such as when switching between files in a list of audio titles.
*
* - DMSManager supports 1, 2, 6 (5.1) or 8 (7.1) channel audio data.
*
* If more than one channel, audio samples must be interleaved into a single incoming audio stream.
* Specifically each channel's sample for t[0] will appear in sequence, followed by
* each channel's sample for t[1], etc.
*
* Within DMSManager, if multiple channels of audio are present, they will be split apart and buffered 
* separately, with each channel sent independently through the audio readers.  In many
* cases this will result in duplicate watermarks being detected and reported to the
* application.  The use of DMSManager audioPayloadCache features are recommended to 
* focus application attention on only new payload detections.
*
* @ingroup DMSAudioSourceProtocolGroup
*/
- (int) numChannels;

@end

/*--------------------------------------------------------------------------------------*/
/** @protocol DMSAudioVisualizerProtocol
 * See the @ref DMSAudioVisualizerProtocolGroup "DMSAudioVisualizerProtocol API" for complete documentation of this protocol.
 */
/** @defgroup DMSAudioVisualizerProtocolGroup DMSDK Audio Visualizer Protocol
 * The DMSAudioVisualizerProtocol defines the interface for relaying audio media data between an audioSource and an associated visualizer view.
 * This is in parallel with the normal DMSDK interface for relaying similar media data into the DMSDK audioWatermark readers.
 */
@protocol DMSAudioVisualizerProtocol <NSObject>
@required

/** Route media source data to delegate for use by visualization view
 *
 * @param data array sequence of 32-bit float audio samples for visualization
 * @param count number of samples in array
 * @ingroup DMSAudioVisualizerProtocolGroup
 */
-(void) audioVisualizerDataAvailableWithData:(float *)data count:(int)count;

@end




